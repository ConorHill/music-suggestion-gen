import React, { Component } from "react";
import { connect } from "react-redux";

import {login} from './store/spotifyActions';
import "./App.css";

class App extends Component {
  
  componentDidMount() {
    this.props.start();
  }

  render() {
    return <div className="App"></div>;
  }
}

const mapStateToProps = (state) => ({
  
})

const mapDispatchToProps = dispatch => ({
  start: () => dispatch(login())
  
})


export default connect(mapStateToProps, mapDispatchToProps)(App);
