import * as actionTypes from "./actionTypes";
import * as credentials from "../credentials";
import axios from "axios";

const loginStart = () => ({
  type: actionTypes.LOGIN_SPOTIFY
});

export const login = () => {
  const headers = {
    "Content-Type": "application/x-www-form-urlencoded",
    'Authorization': `Basic\u00A0${btoa(
        credentials.clientID+":"+credentials.clientSecret
      )}`
  };
  return dispatch => {
    dispatch(loginStart());

    axios
      .post(
        "https://accounts.spotify.com/api/token",
        { grant_type: "client_credentials" },
        { headers: headers }
      )
      .then((res, req) => console.log(res.data));
    // console.log(`Basic\u00A0${btoa(
    //     credentials.clientID+":"+credentials.clientSecret
    //   )}`)
  };
};
