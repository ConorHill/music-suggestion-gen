import * as actionTypes from './actionTypes';
//import {update} from '../shared/utilities';

const initialState = {
    loggedIn: false,
    error: false,
    loading: false
}

const startLogin = (state, action) => {
    return {...state, loading: true}
}



const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.LOGIN_SPOTIFY:
            return startLogin(state, action);
        default:
            return state;
    }
}

export default reducer;